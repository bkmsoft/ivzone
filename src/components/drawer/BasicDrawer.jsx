import {defineComponent, inject, provide, ref} from "vue";
import {LinkViewContextKey, LinkViewPosition, PositionConst, VisibleContextKey} from "@/utils/ProvideKeys";
import {VisibleContext} from "@/components/view/Context";
import MixinsVisible from "@/components/mixins/MixinsVisible";

export default defineComponent({
    name: 'UDrawer',
    props: {
        title: String,
        bodyStyle: Object,
        centered: Boolean,
        afterClose: Function,
        width: {default: 680},
        height: {default: 452},
        zIndex: {default: 868},
        keyboard: {default: true},
        closable: {default: true},
        forceRender: {default: false},
        placement: {default: 'right'},
        maskClosable: {default: true},
        headerStyle: {type: Object},
        destroyOnClose: {default: false},
        uid: {type: String, required: true},
        modelValue: {type: Object},
        'onUpdate:modelValue': {type: Function},
        footerStyle: {type: Object, default: () => { return {textAlign: 'center'}}}
    },
    mixins: [MixinsVisible],
    setup(props) {
        let formRef = ref(null);
        let visible = ref(false);
        let spinning = ref(false);
        let routerCall = () => null;
        let spinTip = ref("");

        /**
         * @type {LinkContext}
         */
        let linkContext = inject(LinkViewContextKey);
        let visibleContext = new VisibleContext(linkContext);
        if(linkContext) {
            visibleContext.uid = props.uid;
            linkContext.addChildrenContext(visibleContext);
        }

        provide(VisibleContextKey, visibleContext);
        provide(LinkViewPosition, PositionConst.Drawer);
        return {formRef, spinning, spinTip, visible, routerCall, linkContext, visibleContext}
    },
    render() {
        return(<ADrawer v-model={[this.visible, 'visible', ["modifier"]]} style={{position: 'absolute'}}
                {...this.$props} v-slots={this.$slots} getContainer=".u-main-container" class="u-drawer" />)
    },
    mounted() {
        this.routerCall = this.$router.beforeEach((from, to, next) => {
            if(this.visible) {
                this.visible = false;
            }

            next();
        })
    },
    unmounted() {
        this.routerCall();
    }
})

